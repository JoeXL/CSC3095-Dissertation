#pragma once

#include "../../nclgl/OGLRenderer.h"
#include "../../nclgl/Camera.h"
#include "../../nclgl/OBJMesh.h"
#include "../../nclgl/SceneNode.h"
#include "../../nclgl/Frustum.h"
#include "../../nclgl/HeightMap.h"
#include <algorithm>
#include <ctime>
//#include <Query.h>

#define POST_PASSES 10

class Renderer : public OGLRenderer {
public:
	Renderer(Window& parent);
	virtual ~Renderer(void);

	virtual void RenderScene();
	virtual void UpdateScene(float msec);

	vector<float> ssaoTimes;
	vector<float> GbufferTimes;
	bool timing = false;

protected:
	void FillBuffers();
	void ApplySSAO();
	void blurSSAO();
	void CombineBuffers();
	void GenerateScreenTexture(GLuint &into, bool depth = false);
	void GenerateNoiseTexture(GLuint& into);
	void GenerateSSAOTexture(GLuint& into);
	void GeneratePosTexture(GLuint& into);

	void CheckSceneChangeInput();
	void GenerateScene(int number);
	void addObject(int sceneNum, Mesh* mesh, Matrix4 transform);
	void SortNodeLists();

	void buildNodeLists(SceneNode* from);
	void clearNodeLists();
	void drawNodes();
	void drawNode(SceneNode* n);

	float lerp(float a, float b, float f) {
		return a + f * (b - a);
	}

	void checkTimerToggle();

	void startTimer();
	float stopTimer();

	Shader* sceneShader;
	Shader* ssaoShader;
	Shader* blurShader;
	Shader* combineShader;

	SceneNode* scenes[5];

	Camera* camera;
	Mesh* quad;

	vector<SceneNode*> nodeList;
	vector<SceneNode*> transparentNodeList;

	Frustum frameFrustum;

	GLuint bufferFBO;
	GLuint bufferColourTex;
	GLuint bufferNormalTex;
	GLuint bufferDepthTex;
	GLuint bufferPosTex;

	GLuint ssaoFBO;
	GLuint blurFBO;
	GLuint bufferSSAOTex;
	GLuint bufferBlurTex;
	GLuint noiseTex;

	vector<Vector3> ssaoNoise;

	int numScenes = 5;
	int currentScene = 0;

	int kernelSize = 64;
	std::vector<Vector3> kernel;
	int noiseSize = 16;
	float radius = 0.3f;
	std::vector<Vector3> noise;

	LARGE_INTEGER frequency;        // ticks per second
	LARGE_INTEGER t1, t2;           // ticks

};