#pragma once

#include "stdafx.h"

#include "../../nclgl/window.h"
#include "Renderer.h"
//#include <Query.h>
#include <iostream>
#include <fstream>

int main() {
	//Window w("Post Processing!", 960, 540, true);
	//Window w("Post Processing!", 1280, 720, true);
	//Window w("Post Processing!", 1600, 900, true);
	//Window w("Post Processing!", 1920, 1080, true);
	Window w("Post Processing!", 2240, 1260, false);
	//Window w("Post Processing!", 2560, 1440, false);
	if (!w.HasInitialised()) {
		return -1;
	}

	Renderer renderer(w);
	if (!renderer.HasInitialised()) {
		return -1;
	}

	w.LockMouseToWindow(true);
	w.ShowOSPointer(false);

	ofstream renderData;
	renderData.open("renderData.txt");

	while (w.UpdateWindow() && !Window::GetKeyboard()->KeyDown(KEYBOARD_ESCAPE)) {
		renderer.UpdateScene(w.GetTimer()->GetTimedMS());
		renderer.RenderScene();

		if (Window::GetKeyboard()->KeyTriggered(KEYBOARD_T)) {
			renderer.timing = true;
			cout << "Starting timer..." << endl;
		}

		if (renderer.ssaoTimes.size() == 100) {
			renderer.timing = false;

			double gBufferAverage = 0;
			for (int i = 0; i < renderer.GbufferTimes.size(); ++i) {
				gBufferAverage += renderer.GbufferTimes[i];
			}
			gBufferAverage /= renderer.GbufferTimes.size();
			cout << "Average render for time for G-Buffer was: " << gBufferAverage << "ms" << endl;
			renderData << "Average render for time for G-Buffer was: " << gBufferAverage << "ms\n";
			renderer.GbufferTimes.clear();

			double ssaoAverage = 0;
			for (int i = 0; i < renderer.ssaoTimes.size(); ++i) {
				ssaoAverage += renderer.ssaoTimes[i];
			}
			ssaoAverage /= renderer.ssaoTimes.size();
			cout << "Average render time for SSAO was: " << ssaoAverage << "ms" << endl;
			renderData << "Average render time for SSAO was: " << ssaoAverage << "ms\n";
			renderer.ssaoTimes.clear();
		}
	}

	renderData.close();

	return 0;
}