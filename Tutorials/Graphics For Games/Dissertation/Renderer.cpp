#include "stdafx.h"
#include "Renderer.h"
#include <random>

Renderer::Renderer(Window& parent) : OGLRenderer(parent) {

	currentScene = 0;

	camera = new Camera(0.0f, 135.0f, Vector3(15, 5, -15));


	for (int i = 0; i < numScenes; ++i) {
		GenerateScene(i);
	}

	std::uniform_real_distribution<float> randomFloats(0.0, 1.0);
	std::default_random_engine generator;
	//std::random_device generator;

	for (int i = 0; i < kernelSize; ++i) {
		Vector3 sample(
			randomFloats(generator) * 2.0 - 1.0,
			randomFloats(generator) * 2.0 - 1.0,
			randomFloats(generator));
		sample.Normalise();
		sample = sample * randomFloats(generator);
		float scale = (float)i / kernelSize;

		scale = lerp(0.1f, 1.0f, scale * scale);
		sample = sample * scale;
		kernel.push_back(sample);
	}

	for (int i = 0; i < noiseSize; ++i) {
		Vector3 noiseVec(
			randomFloats(generator) * 2.0 - 1.0,
			randomFloats(generator) * 2.0 - 1.0,
			0.0f);
		noise.push_back(noiseVec);
	}

	quad = Mesh::GenerateQuad();

	sceneShader = new Shader(SHADERDIR"SSAO/bumpVertex.glsl",
		SHADERDIR"SSAO/bufferFragment.glsl");
	ssaoShader = new Shader(SHADERDIR"SSAO/ssaoVert.glsl",
		SHADERDIR"SSAO/ssaoFrag.glsl");
	blurShader = new Shader(SHADERDIR"SSAO/combineVert.glsl",
		SHADERDIR"SSAO/blurFrag.glsl");
	combineShader = new Shader(SHADERDIR"SSAO/combinevert.glsl",
		SHADERDIR"SSAO/combinefrag.glsl");

	if (!sceneShader->LinkProgram() || !ssaoShader->LinkProgram() || !combineShader->LinkProgram() || !blurShader->LinkProgram()) {
		return;
	}

	glGenFramebuffers(1, &bufferFBO);
	glGenFramebuffers(1, &ssaoFBO);
	glGenFramebuffers(1, &blurFBO);

	GLenum buffers[3];
	buffers[0] = GL_COLOR_ATTACHMENT0;
	buffers[1] = GL_COLOR_ATTACHMENT1;
	buffers[2] = GL_COLOR_ATTACHMENT2;

	//Generate screen textures
	GenerateScreenTexture(bufferDepthTex, true);
	GenerateScreenTexture(bufferColourTex);
	GenerateScreenTexture(bufferNormalTex);
	GenerateScreenTexture(bufferPosTex);
	GenerateScreenTexture(bufferSSAOTex);
	GenerateScreenTexture(bufferBlurTex);
	GenerateSSAOTexture(bufferSSAOTex);
	GenerateNoiseTexture(noiseTex);

	glBindFramebuffer(GL_FRAMEBUFFER, bufferFBO);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, bufferColourTex, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, bufferNormalTex, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, bufferPosTex, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, bufferDepthTex, 0);
	glDrawBuffers(3, buffers);

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
		return;
	}

	GLenum ssaoBuffers[1];
	ssaoBuffers[0] = GL_COLOR_ATTACHMENT0;

	glBindFramebuffer(GL_FRAMEBUFFER, ssaoFBO);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, bufferSSAOTex, 0);
	glDrawBuffers(1, ssaoBuffers);

	GLenum blurBuffers[1];
	blurBuffers[0] = GL_COLOR_ATTACHMENT0;

	glBindFramebuffer(GL_FRAMEBUFFER, blurFBO);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, bufferBlurTex, 0);
	glDrawBuffers(1, blurBuffers);

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
		return;
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glEnable(GL_BLEND);

	init = true;
}

Renderer::~Renderer(void) {
	currentShader = NULL;
	delete sceneShader;
	delete ssaoShader;
	delete combineShader;
	delete camera;
	delete quad;


	glDeleteTextures(1, &bufferColourTex);
	glDeleteTextures(1, &bufferNormalTex);
	glDeleteTextures(1, &bufferDepthTex);
	glDeleteTextures(1, &bufferSSAOTex);

	glDeleteFramebuffers(1, &bufferFBO);
	glDeleteFramebuffers(1, &ssaoFBO);
}

void Renderer::UpdateScene(float msec) {
	camera->UpdateCamera(msec);
	CheckSceneChangeInput();
	viewMatrix = camera->BuildViewMatrix();
	frameFrustum.FromMatrix(projMatrix*viewMatrix);
}

void Renderer::startTimer() {
	QueryPerformanceFrequency(&frequency);
	QueryPerformanceCounter(&t1);
}

float Renderer::stopTimer() {
	QueryPerformanceCounter(&t2);
	return (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;
}

void Renderer::RenderScene() {
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

	buildNodeLists(scenes[currentScene]);
	SortNodeLists();

	
	if (timing) {

		startTimer();

		FillBuffers();

		float gbuffElapsedTime = stopTimer();
		
		startTimer();

		ApplySSAO();
		blurSSAO();

		float ssaoElapsedTime = stopTimer();
		GbufferTimes.push_back(gbuffElapsedTime);
		ssaoTimes.push_back(ssaoElapsedTime);
	}
	else {
		FillBuffers();
		ApplySSAO();
		blurSSAO();
	}
	CombineBuffers();
	SwapBuffers();
	clearNodeLists();

}

void Renderer::CheckSceneChangeInput() {
	if (Window::GetKeyboard()->KeyTriggered(KEYBOARD_1)) {
		currentScene = 0;
		camera->SetPosition(Vector3(6.933, 1.664, -15.395));
		camera->SetYaw(142.28);
		camera->SetPitch(-12.67);
	}
	if (Window::GetKeyboard()->KeyTriggered(KEYBOARD_2)) {
		currentScene = 1;
		camera->SetPosition(Vector3(0.01, 4.583, 1.402));
		camera->SetYaw(1.51);
		camera->SetPitch(-56.98);
	}
	if (Window::GetKeyboard()->KeyTriggered(KEYBOARD_3)) {
		currentScene = 2;
		camera->SetPosition(Vector3(6.412, 1.162, -14.459));
		camera->SetYaw(143.4);
		camera->SetPitch(-16.73);
	}
	if (Window::GetKeyboard()->KeyTriggered(KEYBOARD_4)) {
		currentScene = 3;
	}
	if (Window::GetKeyboard()->KeyTriggered(KEYBOARD_5)) {
		currentScene = 4;
	}
	if (Window::GetKeyboard()->KeyDown(KEYBOARD_UP)) {
		radius += 0.1;
	}
	if (Window::GetKeyboard()->KeyDown(KEYBOARD_DOWN)) {
		if (radius > 0) {
			radius -= 0.1;
		}
	}

}

void Renderer::GenerateScene(int number) {
	SceneNode* temp = new SceneNode(new Mesh());
	Mesh* floor = Mesh::GenerateQuad();

	floor->SetTexture(SOIL_load_OGL_texture(TEXTUREDIR"strawberry.png",
		SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS));
	floor->SetBumpMap(SOIL_load_OGL_texture(TEXTUREDIR"strawberry_bump.jpg", SOIL_LOAD_AUTO,
		SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS));

	SetTextureRepeating(floor->GetTexture(), true);
	SetTextureRepeating(floor->GetBumpMap(), true);
	SceneNode* base = new SceneNode(floor);
	base->SetTransform(
		Matrix4::Scale(Vector3(500, 1, 500)) *
		Matrix4::Translation(Vector3(0, 0, 0)) *
		Matrix4::Rotation(90, Vector3(1, 0, 0)));
	base->SetBoundingRadius(2000.0f);
	temp->AddChild(base);

	if(number == 0) {
		Mesh* mesh = (Mesh*)(new OBJMesh(MESHDIR"cube2.obj"));
		mesh->SetTexture(SOIL_load_OGL_texture(TEXTUREDIR"strawberry.png",
			SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS));
		mesh->SetBumpMap(SOIL_load_OGL_texture(TEXTUREDIR"strawberry_bump.jpg", SOIL_LOAD_AUTO,
			SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS));

		SetTextureRepeating(mesh->GetTexture(), true);
		SetTextureRepeating(mesh->GetBumpMap(), true);

		int scale = 4;
		int rows = 4 * 4;
		int columns = 4 * 8;
		for (int i = 0; i < rows; ++i) {
			for (int j = 0; j < columns; ++j) {
				SceneNode* foo = new SceneNode(mesh);
				foo->SetTransform(
					Matrix4::Scale(Vector3(0.2, 0.2, 0.2)) *
					Matrix4::Translation(Vector3(
						-(((rows) / 2) * scale) + (i * scale),
						0.5,
						-((columns / 2) * scale) + (j * scale))) *
					Matrix4::Scale(Vector3(2, 2, 2)) *
					Matrix4::Rotation(90, Vector3(-1, 0, 0)));

				foo->SetBoundingRadius(2000.0f);
				temp->AddChild(foo);

			}
		}
	}
	else if (number == 1) {
		Mesh* mesh = (Mesh*)(new OBJMesh(MESHDIR"nanosuit.obj"));
		mesh->SetTexture(SOIL_load_OGL_texture(TEXTUREDIR"strawberry.png",
			SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS));
		mesh->SetBumpMap(SOIL_load_OGL_texture(TEXTUREDIR"strawberry_bump.jpg", SOIL_LOAD_AUTO,
			SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS));

		SetTextureRepeating(mesh->GetTexture(), true);
		SetTextureRepeating(mesh->GetBumpMap(), true);

		SceneNode* foo = new SceneNode(mesh);
		foo->SetTransform(Matrix4::Scale(Vector3(0.2, 0.2, 0.2)));

		foo->SetBoundingRadius(2000.0f);
		temp->AddChild(foo);
	}
	else if (number == 2) {
		Mesh* mesh = (Mesh*)(new OBJMesh(MESHDIR"Sphere.obj"));
		mesh->SetTexture(SOIL_load_OGL_texture(TEXTUREDIR"strawberry.png",
			SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS));
		mesh->SetBumpMap(SOIL_load_OGL_texture(TEXTUREDIR"strawberry_bump.jpg", SOIL_LOAD_AUTO,
			SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS));

		SetTextureRepeating(mesh->GetTexture(), true);
		SetTextureRepeating(mesh->GetBumpMap(), true);

		int scale = 2;
		int rows = 4 * 4;
		int columns = 4 * 8;
		for (int i = 0; i < rows; ++i) {
			for (int j = 0; j < columns; ++j) {
				SceneNode* foo = new SceneNode(mesh);
				foo->SetTransform(
					Matrix4::Scale(Vector3(0.4, 0.4, 0.4)) *
					Matrix4::Translation(Vector3(
						-((rows / 2) * scale) + (i * scale),
						0.9,
						-((columns / 2) * scale) + (j * scale))));
				foo->SetBoundingRadius(2000.0f);
				temp->AddChild(foo);
			}
		}
	}
	scenes[number] = temp;
}

void Renderer::addObject(int sceneNum, Mesh* mesh, Matrix4 transform) {
	mesh->SetTexture(SOIL_load_OGL_texture(TEXTUREDIR"Barren Reds.jpg",
		SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS));
	mesh->SetBumpMap(SOIL_load_OGL_texture(TEXTUREDIR"Barren RedsDOT3.tga", SOIL_LOAD_AUTO,
		SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS));
	SetTextureRepeating(mesh->GetTexture(), true);
	SetTextureRepeating(mesh->GetBumpMap(), true);
	SceneNode* temp = new SceneNode(mesh);
	temp->SetBoundingRadius(200.0f);
	temp->SetTransform(transform);
	scenes[sceneNum]->AddChild(temp);
}

void Renderer::SortNodeLists() {
	std::sort(transparentNodeList.begin(), transparentNodeList.end(), SceneNode::CompareByCameraDistance);
	std::sort(nodeList.begin(), nodeList.end(), SceneNode::CompareByCameraDistance);
}

void Renderer::buildNodeLists(SceneNode* from) {
	if (frameFrustum.InsideFrustum(*from)) {
		Vector3 dir = from->GetWorldTransform().GetPositionVector() - camera->GetPosition();
		from->SetCameraDistance(Vector3::Dot(dir, dir));

		if (from->GetColour().w < 1.0f) {
			transparentNodeList.push_back(from);
		}
		else {
			nodeList.push_back(from);
		}
	}

	for (vector<SceneNode*>::const_iterator i = from->GetChildIteratorStart(); i != from->GetChildIteratorEnd(); ++i) {
		buildNodeLists((*i));
	}
}

void Renderer::clearNodeLists() {
	transparentNodeList.clear();
	nodeList.clear();
}

void Renderer::drawNodes() {
	for (vector<SceneNode*>::const_iterator i = nodeList.begin(); i != nodeList.end(); ++i) {
		drawNode((*i));
	}
}

void Renderer::drawNode(SceneNode* n) {
	if (n->GetMesh()) {
		glUniformMatrix4fv(glGetUniformLocation(
			currentShader->GetProgram(), "modelMatrix"), 1, false, (float*)&(n->GetTransform()));
		glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "diffuseTex"), 0);
		glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "bumpTex"), 1);
		n->Draw();
	}
}

void Renderer::GenerateScreenTexture(GLuint& into, bool depth) {
	glGenTextures(1, &into);
	glBindTexture(GL_TEXTURE_2D, into);

	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	glTexImage2D(GL_TEXTURE_2D, 0,
		depth ? GL_DEPTH_COMPONENT24 : GL_RGBA32F,
		width, height, 0,
		depth ? GL_DEPTH_COMPONENT : GL_RGBA,
		GL_UNSIGNED_BYTE, NULL);

	if (depth) {
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	}

	glBindTexture(GL_TEXTURE_2D, 0);
}

void Renderer::GeneratePosTexture(GLuint& into) {
	glGenTextures(1, &into);
	glBindTexture(GL_TEXTURE_2D, into);

	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, width, height, 0, GL_RGBA32F, GL_UNSIGNED_BYTE, NULL);

	glBindTexture(GL_TEXTURE_2D, 0);
}

void Renderer::GenerateNoiseTexture(GLuint& into) {
	glGenTextures(1, &into);
	glBindTexture(GL_TEXTURE_2D, into);

	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, 4, 4, 0, GL_RGB, GL_FLOAT, &noise[0]);

	glBindTexture(GL_TEXTURE_2D, 0);
}

void Renderer::GenerateSSAOTexture(GLuint& into) {
	glGenTextures(1, &into);
	glBindTexture(GL_TEXTURE_2D, into);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, width, height, 0, GL_RGB, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glBindTexture(GL_TEXTURE_2D, 0);
}

void Renderer::FillBuffers() {
	glBindFramebuffer(GL_FRAMEBUFFER, bufferFBO);
	glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

	SetCurrentShader(sceneShader);
	glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "diffuseTex"), 0);
	glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "bumpTex"), 1);

	projMatrix = Matrix4::Perspective(0.1f, 5000.0f, (float)width / (float)height, 45.0f);
	modelMatrix.ToIdentity();
	UpdateShaderMatrices();

	drawNodes();

	glUseProgram(0);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void Renderer::ApplySSAO() {
	SetCurrentShader(ssaoShader);
	glBindFramebuffer(GL_FRAMEBUFFER, ssaoFBO);
	glClearColor(0, 0, 0, 1);
	projMatrix = Matrix4::Perspective(0.1f, 5000.0f, (float)width / (float)height, 45.0f);
	glUniformMatrix4fv(glGetUniformLocation(currentShader->GetProgram(), "originalViewMatrix"), 1, false, (float*)&viewMatrix);
	glUniformMatrix4fv(glGetUniformLocation(currentShader->GetProgram(), "originalProjMatrix"), 1, false, (float*)&projMatrix);

	projMatrix = Matrix4::Orthographic(-1, 1, 1, -1, -1, 1);
	viewMatrix.ToIdentity();

	glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "depthTex"), 3);
	glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "normTex"), 4);
	glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "noiseTex"), 5);
	glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "posTex"), 6);

	UpdateShaderMatrices();

	glDisable(GL_DEPTH_TEST);

	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, bufferDepthTex);

	glActiveTexture(GL_TEXTURE4);
	glBindTexture(GL_TEXTURE_2D, bufferNormalTex);

	glActiveTexture(GL_TEXTURE5);
	glBindTexture(GL_TEXTURE_2D, noiseTex);

	glActiveTexture(GL_TEXTURE6);
	glBindTexture(GL_TEXTURE_2D, bufferPosTex);


	glUniform2f(glGetUniformLocation(currentShader->GetProgram(), "noiseScale"), width / 4.0, height / 4.0);
	glUniform4fv(glGetUniformLocation(currentShader->GetProgram(), "cameraPos"),
		1, (float*)&camera->GetPosition());
	glUniform2f(glGetUniformLocation(currentShader->GetProgram(),
		"pixelSize"), 1.0f / width, 1.0f / height);
	glUniform1f(glGetUniformLocation(currentShader->GetProgram(), "radius"), radius);
	glUniform3fv(glGetUniformLocation(currentShader->GetProgram(), "sampleKernel"), kernel.size(), reinterpret_cast<float*>(kernel.data()));
	quad->Draw();

	glUseProgram(0);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	glEnable(GL_DEPTH_TEST);
}

void Renderer::blurSSAO() {
	SetCurrentShader(blurShader);
	glBindFramebuffer(GL_FRAMEBUFFER, blurFBO);
	glClearColor(0, 0, 0, 1);
	glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

	projMatrix = Matrix4::Orthographic(-1, 1, 1, -1, -1, 1);
	viewMatrix.ToIdentity();
	UpdateShaderMatrices();

	glDisable(GL_DEPTH_TEST);

	glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "ssaoTex"), 7);

	glActiveTexture(GL_TEXTURE7);
	glBindTexture(GL_TEXTURE_2D, bufferSSAOTex);

	quad->Draw();

	glUseProgram(0);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glEnable(GL_DEPTH_TEST);
}

void Renderer::CombineBuffers() {
	SetCurrentShader(combineShader);
	glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

	projMatrix = Matrix4::Orthographic(-1, 1, 1, -1, -1, 1);
	UpdateShaderMatrices();

	glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "diffuseTex"), 8);
	glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "blurTex"), 9);

	glActiveTexture(GL_TEXTURE8);
	glBindTexture(GL_TEXTURE_2D, bufferColourTex);

	glActiveTexture(GL_TEXTURE9);
	glBindTexture(GL_TEXTURE_2D, bufferBlurTex);

	quad->Draw();

	glUseProgram(0);
}