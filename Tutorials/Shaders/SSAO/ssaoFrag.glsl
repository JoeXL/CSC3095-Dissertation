#version 330 core

#define KERNEL_SIZE 64

uniform sampler2D depthTex;
uniform sampler2D normTex;
uniform sampler2D noiseTex;
uniform sampler2D posTex;

uniform mat4 originalProjMatrix;
uniform mat4 originalViewMatrix;
uniform mat4 projMatrix;
uniform mat4 viewMatrix;
uniform vec2 noiseScale;
uniform vec2 pixelSize;
uniform float radius;
uniform vec3 sampleKernel[KERNEL_SIZE];

in Vertex {
	vec2 texCoord;
	mat4 inverseProjView;
	mat4 projMatrix;
} IN;

out vec4 fragColor;

float linearizeDepth(vec2 uv) {
	float f = 50.0;
	float n = 0.1;
	float z = texture(depthTex, uv).r * 2.0 - 1.0;
	return (2 * n) / (f + n - z * (f - n));
}

float linearizeDepth(float z) {
	float f = 50.0;
	float n = 0.1;
	return (2 * n) / (f + n - z * (f - n));
}


vec3 getViewPos(vec2 texCoord) {
	float x = (IN.texCoord.x * 2.0 - 1.0);
	float y = (IN.texCoord.y * 2.0 - 1.0);
	float z = (linearizeDepth(IN.texCoord));
	//float z = texture(depthTex, IN.texCoord).r;
	
	vec4 pos = vec4(x,y,z,1.0);
	
	vec4 posView = IN.inverseProjView * pos;
	
	return posView.xyz / posView.w;
}

void main(void) {
	vec3 posView = texture(posTex, IN.texCoord).xyz;
	
	vec3 normalView = normalize(texture(normTex, IN.texCoord).xyz);
	
	vec3 randomVector = texture(noiseTex, IN.texCoord * noiseScale).xyz;
	
	vec3 tangentView = normalize(randomVector - dot(randomVector, normalView) * normalView);
	
	vec3 bitangentView = cross(normalView, tangentView);
	
	mat3 TBN = mat3(tangentView, bitangentView, normalView);
	
	float occlusion = 0.0;
	
	for(int i = 0; i < KERNEL_SIZE; ++i) {
		// Reorient sample vector in view space
		vec3 sampleVectorView = TBN * sampleKernel[i];
		
		// Calculate sample position
		vec3 samplePointView = posView + sampleVectorView * radius;
		
		// Project point and calculate NDC
		vec4 samplePointNDC = originalProjMatrix * vec4(samplePointView, 1.0);
		samplePointNDC /= samplePointNDC.w;
		
		// Create texture coordinate
		vec2 samplePointTexCoord = samplePointNDC.xy * 0.5 + 0.5;
		
		// Get sample depth out of depth texture
		float sampleDepth = texture(posTex, samplePointTexCoord).z;
		
		// Check depths and increment occlusion
		float rangeCheck = smoothstep(0.0, 1.0, (radius * 0.2) / abs(posView.z - sampleDepth));
		occlusion += (sampleDepth >= samplePointView.z ? 1.0 : 0.0) * rangeCheck;
	}
	
	occlusion = 1.0 - occlusion / KERNEL_SIZE;
	
	fragColor = vec4(occlusion, occlusion, occlusion, 1.0);
}