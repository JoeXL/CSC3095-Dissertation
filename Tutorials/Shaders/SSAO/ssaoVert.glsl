#version 150 core

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projMatrix;
uniform mat4 textureMatrix;

uniform mat4 originalProjMatrix;
uniform mat4 originalViewMatrix;

in vec3 position;
in vec2 texCoord;

out Vertex {
	vec2 texCoord;
	mat4 inverseProjView;
	mat4 projMatrix;
} OUT;

/*out Vertex {
	vec2 TexCoords;
} OUT;*/

void main(void) {
	mat4 mvp = projMatrix * viewMatrix * modelMatrix;
	gl_Position = mvp * vec4(position, 1.0);
	OUT.texCoord = (textureMatrix * vec4(texCoord, 0.0, 1.0)).xy;
	OUT.inverseProjView = inverse(projMatrix*viewMatrix);
	//OUT.inverseProjView = inverse(originalProjMatrix);
	OUT.projMatrix = projMatrix;
}