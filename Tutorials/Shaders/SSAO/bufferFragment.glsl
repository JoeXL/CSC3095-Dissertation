#version 150 core

uniform sampler2D diffuseTex;
uniform sampler2D bumpTex;

in Vertex{
	vec4 colour;
	vec2 texCoord;
	vec3 normal;
	vec3 tangent;
	vec3 binormal;
	vec3 worldPos;
	vec3 FragPos;
	vec3 normalViewSpace;
} IN;

out vec4 fragColor[3];

void main(void) {
	mat3 TBN = mat3(IN.tangent, IN.binormal, IN.normal);
	vec3 normal = normalize(TBN * (texture2D(bumpTex, IN.texCoord).rgb)*2.0 - 1.0);
	
	//normal = -normal;
	//normal = IN.normal;
	
	//vec3 normal = normalize(IN.normal);

	fragColor[0] = texture2D(diffuseTex, IN.texCoord);
	//fragColor[1] = vec4(normal.xyz * 0.5 + 0.5, 1.0);
	//fragColor[1] = vec4(IN.normal.xyz * 0.5 + 0.5, 1.0);
	fragColor[1] = vec4(IN.normalViewSpace.xyz, 1.0);
	//fragColor[1] = vec4(normal.xyz, 1.0);
	//fragColor[1] = vec4(IN.tangent.xyz, 1.0);
	//fragColor[1] = vec4(1.0, 0.5, 0.0, 1.0);
	fragColor[2] = vec4(IN.FragPos.xyz, 1.0);
	//fragColor[2] = vec4(IN.worldPos.xyz, 1.0);
}