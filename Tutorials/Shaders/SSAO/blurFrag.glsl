#version 330 core

uniform sampler2D ssaoTex;

in Vertex {
	vec2 texCoord;
} IN;

out vec4 fragColor;

void main(void) {
	vec2 texelSize = 1.0 / vec2(textureSize(ssaoTex, 0));
	int radius = 2;
	float result = 0.0;
	for(int x = -radius; x < radius; ++x) {
		for(int y = -radius; y < radius; ++y) {
			vec2 offset = vec2(float(x), float(y)) * texelSize;
			result += texture(ssaoTex, IN.texCoord + offset).r;
		}
	}
	result = result / (16);
	fragColor = vec4(result,result,result,1.0);
	//fragColor = vec4(texture(ssaoTex, IN.texCoord).xyz, 1.0);
}