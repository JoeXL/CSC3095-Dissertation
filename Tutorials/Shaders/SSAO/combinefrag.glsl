#version 150 core
uniform sampler2D diffuseTex;
uniform sampler2D blurTex;

in Vertex{
	vec2 texCoord;
} IN;

out vec4 fragColor;

void main(void) {
	vec3 diffuse = texture(diffuseTex, IN.texCoord).xyz;
	vec3 occlusion = texture(blurTex, IN.texCoord).xyz;

	//fragColor.xyz = diffuse * (occlusion * 0.5 + 0.2); //ao
	//fragColor.xyz = diffuse * occlusion; //ao
	fragColor.xyz = occlusion;
	//fragColor.xyz = diffuse;
	fragColor.a = 1.0;
}