# Stage 3 Dissertation
## Project Overview
### Objective
The aim of the project was a performance analysis of SSAO

### Video Demo

https://www.youtube.com/watch?v=dTprW0uIyEE&feature=youtu.be

### Controls
* WSAD - Move the camera
* Mouse - Rotate the camera
* Space - Move the camera up
* C - Move the camera down
* Up,Down - Adjust the radius of the SSAO

## Build Information
The solution file is in the **Tutorials/Graphics For Games** folder

It is currently only configured for Win32

Startup project should be set to **Dissertation**

Build directories are in **Tutorials/Graphics For Games** then either **Release** or **Debug**
